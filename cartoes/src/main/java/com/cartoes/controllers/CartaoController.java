package com.cartoes.controllers;

import com.cartoes.DTOs.CartaoAtivoDTO;
import com.cartoes.DTOs.CartaoGetDTO;
import com.cartoes.models.Cartao;
import com.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao criarCartao(@RequestBody @Valid Cartao cartao){
        try {
            Cartao cartaoObjeto = cartaoService.criarCartao(cartao);
            return cartaoObjeto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{id}")
    public CartaoGetDTO bucarPorId(@PathVariable(name = "id") int id){
        try{
            CartaoGetDTO cartao = cartaoService.buscarPorId(id);
            return cartao;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PatchMapping("/{id}")
    public Cartao ativaOuDesativaCartao(@PathVariable(name = "id") int id, @RequestBody CartaoAtivoDTO cartaoDTO){
        try{
            Cartao cartaoObjeto = cartaoService.ativarOuDesativarCartao(id, cartaoDTO);
            return cartaoObjeto;
        }catch (RuntimeException exception){
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
