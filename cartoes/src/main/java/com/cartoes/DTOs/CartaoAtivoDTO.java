package com.cartoes.DTOs;

public class CartaoAtivoDTO {
    private boolean ativo;

    public CartaoAtivoDTO() {
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
