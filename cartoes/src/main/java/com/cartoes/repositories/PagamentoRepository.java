package com.cartoes.repositories;

import com.cartoes.models.Cliente;
import com.cartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Iterable<Pagamento> findAllByCartaoId(int id);
}
