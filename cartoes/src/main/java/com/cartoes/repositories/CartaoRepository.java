package com.cartoes.repositories;

import com.cartoes.models.Cartao;
import com.cartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
}
