package com.cartoes.services;

import com.cartoes.models.Pagamento;
import com.cartoes.repositories.CartaoRepository;
import com.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoRepository cartaoRepository;

    public Pagamento criarPagamento(Pagamento pagamento){
        if (cartaoRepository.existsById(pagamento.getCartaoId())){
            Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
            return pagamentoObjeto;
        }
        throw new RuntimeException("O cartão não foi encontrado");
    }

    public Iterable<Pagamento> buscarTodosPorId(int id){
        Iterable<Pagamento> Pagamentos = pagamentoRepository.findAllByCartaoId(id);
        return Pagamentos;
    }

}
