package com.cartoes.services;

import com.cartoes.DTOs.CartaoAtivoDTO;
import com.cartoes.DTOs.CartaoGetDTO;
import com.cartoes.models.Cartao;
import com.cartoes.repositories.CartaoRepository;
import com.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    public Cartao criarCartao(Cartao cartao){
        if (clienteRepository.existsById(cartao.getClienteId())){
            cartao.setAtivo(false);
            Cartao cartaoObjeto = cartaoRepository.save(cartao);
            return cartaoObjeto;
        }
        throw new RuntimeException("O Cliente não foi encontrado");
    }

    public CartaoGetDTO buscarPorId(int id) {
        Optional<Cartao> optionalCartao = cartaoRepository.findById(id);
        if (optionalCartao.isPresent()){
            CartaoGetDTO cartaoGetDTO = new CartaoGetDTO();

            cartaoGetDTO.setId(optionalCartao.get().getId());
            cartaoGetDTO.setNumero(optionalCartao.get().getNumero());
            cartaoGetDTO.setClienteId(optionalCartao.get().getClienteId());

            return cartaoGetDTO;
        }
        throw new RuntimeException("O cartão não foi encontrado");
    }

    public Cartao ativarOuDesativarCartao(int id, CartaoAtivoDTO cartaoDTO){
        if (cartaoRepository.existsById(id)){
            Optional<Cartao> cartaoAtual = cartaoRepository.findById(id);

            Cartao cartao = new Cartao();
            cartao.setId(cartaoAtual.get().getId());
            cartao.setClienteId(cartaoAtual.get().getClienteId());
            cartao.setNumero(cartaoAtual.get().getNumero());
            cartao.setAtivo(cartaoDTO.isAtivo());

            Cartao cartaoObjeto = cartaoRepository.save(cartao);
            return cartaoObjeto;
        }
        throw new RuntimeException("O cartão não foi encontrado");
    }
}
